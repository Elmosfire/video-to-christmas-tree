# Video To Christmas Tree

A small program designed to convert a video file to a csv that is be able to run on Matt Parkers Christmas tree. 

## Running the script

You need python version 3.8 or newer installed

To install packages, run

```
pip install -r requirements.txt
```

To run use 

```
python convert.py <name of video file>.mp4
```

## Verifying the output

To test your ouput you can use this repository: https://github.com/mazore/parker-tree-player

You can download and run this repository automaticcaly with

```
bash test.sh <output csv file>
```

## Supported video formats

Every video format supported by moviepy/ffmpeg is supported by this script.
