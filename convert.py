import pandas as pd
from pathlib import Path
from moviepy.editor import VideoFileClip
from tqdm import tqdm
import numpy as np
import sys

def get_frame_indices(csvfile: Path, w: int, h: int):
    """
    This function transforms the input coords to match the pixels of the video
    """
    
    df = pd.read_csv(csvfile, header=None)
    df.columns = ["x", "y", "z"]
    
    df = df [["y", "z"]]
    df["z"] *= -1
    
    ymin, ymax = min(df["y"]), max(df["y"])
    zmin, zmax = min(df["z"]), max(df["z"])
    
    
    df["y"] -= ymin
    df["z"] -= zmin
    
    df["y"] /= min(ymax - ymin, zmax - zmin)
    df["z"] /= min(zmax - zmin, zmax - zmin)
    
    assert (df["y"] <= 1).all()
    assert (df["y"] >= 0).all()
    assert (df["z"] <= 1).all()
    assert (df["z"] >= 0).all()
    
    df["y"] *= w
    df["z"] *= h
    
    colors = [0,1,2] * len(df.index)
    yskip = [x for x in df["y"].astype(int) for _ in range(3)]
    zskip = [x for x in df["z"].astype(int) for _ in range(3)]
    
    return colors, yskip, zskip
    
    
def create_video(video: Path, csvfile: Path):
    v = VideoFileClip(str(video))
    cols, xpixs, ypixs = get_frame_indices(csvfile, v.w-1, v.h-1)
    print(v.w-1, v.h-1, 3)
    
    for i, frame in tqdm(enumerate(v.iter_frames(dtype="uint8")), total=v.duration * v.fps):
        data = frame[ypixs, xpixs, cols]
        assert all(0 <= x <= 255 for x in data)
        yield data
        
def build_csv(video: Path, csvfile: Path):
    pd.DataFrame(
        np.stack(create_video(video, csvfile)), 
        columns=
        [x.format(y) for y in range(500) for x in ("R_{}", "G_{}", "B_{}")]
    ).to_csv(video.with_suffix(".csv"),
        index_label="FRAME_ID")
    
    
build_csv(Path(sys.argv[1]), Path("coords_2021.csv"))

    

    